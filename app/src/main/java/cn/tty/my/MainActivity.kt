package cn.tty.my

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import cn.tty.common.CommonUtils

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        CommonUtils.foo()
    }
}